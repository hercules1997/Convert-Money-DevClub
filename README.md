
   # CodeClub-Convert-Money

   Desafio do 9º Módulo do curso DevClub.

   ## Tecnologias utilizadas
    
 <div>
 <img align="center" alt="HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
 <img align="center" alt="CSS3" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
 <img align="center" alt="Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
 </div>
 <hr>
<div>
   <p>Este projeto tem como objetivo de fazer com que o usuário possa realizar conversões de real para outras moedas.</p>
 <a href="http://hercules1997.github.io/CodeClub-Convert-Money/" target="_blank" rel="noopener noreferrer">Página da aplicação</a>
<div>
    <img width="500"
        src="https://user-images.githubusercontent.com/109186074/205403702-2fcaaa25-1f25-4893-acad-e7406d81ca23.png"
        alt="">
    <img width="500"
        src="https://user-images.githubusercontent.com/109186074/205403739-17a28c81-832b-40ac-8bd0-b8cda01cf06d.png"
        alt="">
</div>


